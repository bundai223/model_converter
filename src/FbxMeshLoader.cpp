//**********************************************************************
//! @file   FbxMeshLoader.cpp
//! @brief  describe
//**********************************************************************
#include <iostream>
#include <assert.h>
#include <fbxsdk.h>
#include "FbxMeshLoader.h"
#include "Common/Common.h"


using namespace std;

class FbxMeshLoader::Impl
{
public:
    void LoadMesh(const char* filename);
    void PrintAllMesh() const;

    u32  MeshNum() const{       return meshArray.size(); }
    u32  MaterialNum() const{   return materialArray.size(); }
    u32  BoneNum() const{       return boneArray.size(); }

    const ResMeshData*          Mesh(s32 index) const{      return meshArray[index]; }
    const res::MaterialData*    Material(s32 index) const{  return materialArray[index]; }
    const res::BoneData*        Bone(s32 index) const{      return boneArray[index]; }

private:
    void _LoadFromScene(const KFbxScene& scene);
    void _LoadNode(const FbxNode* node);
    void _LoadMesh(ResMeshData* meshData, const FbxMesh& mesh);
    void _LoadMeshVertices(ResMeshData* meshData, const FbxMesh& mesh);
    void _LoadMeshIndices(ResMeshData* meshData, const FbxMesh& mesh);
    void _ConvMeshIndexQuad2Triangle(PolygonIndex* poly, const FbxMesh& mesh, int polyIndex);

    void _PrintMesh(const ResMeshData* ) const;


    std::vector<ResMeshData*>       meshArray;
    std::vector<res::MaterialData*> materialArray;
    std::vector<res::BoneData*>     boneArray;
};

FbxMeshLoader::FbxMeshLoader()
{
    _Initialize();
}

FbxMeshLoader::~FbxMeshLoader()
{
    _Finalize();
}

void FbxMeshLoader::LoadMesh(const char* filename)
{
    mImpl->LoadMesh(filename);

    mImpl->PrintAllMesh();
}

void FbxMeshLoader::Impl::PrintAllMesh() const
{
	u32 meshNum = meshArray.size();
    for(u32 meshIndex= 0; meshIndex < meshNum; ++meshIndex )
    {
        _PrintMesh(meshArray[meshIndex]);
    }
}

#define GET_NUM(funcname)                                       \
    u32 FbxMeshLoader::funcname() const                         \
    {                                                           \
        return mImpl->funcname();                               \
    }

#define GET_PARAM(ret_type, funcname)                           \
    const ret_type  FbxMeshLoader::funcname(s32 index) const    \
    {                                                           \
        return mImpl->funcname(index);                          \
    }

GET_NUM(MeshNum)
GET_NUM(MaterialNum)
GET_NUM(BoneNum)

GET_PARAM(ResMeshData*,         Mesh)
GET_PARAM(res::MaterialData*,   Material)
GET_PARAM(res::BoneData*,       Bone)

#undef GET_NUM
#undef GET_PARAM

void FbxMeshLoader::_Initialize()
{
    mImpl.reset(new Impl());
}

void FbxMeshLoader::_Finalize()
{
}

void FbxMeshLoader::Impl::LoadMesh(const char* filename)
{
    KFbxSdkManager* sdkManager = NULL;
    KFbxScene* scene = NULL;

    InitializeSdkObjects(sdkManager, scene);

    bool result = LoadScene(sdkManager, scene, filename);

    _LoadFromScene(*scene);

    scene->Destroy();
    DestroySdkObjects(sdkManager, true);
}

void FbxMeshLoader::Impl::_LoadFromScene(const KFbxScene& scene)
{
    const FbxNode* node = scene.GetRootNode();
    
    if(node != NULL)
    {
        const int CHILD_NUM = node->GetChildCount();
        for(int index = 0; index < CHILD_NUM; ++index)
        {
            _LoadNode(node->GetChild(index));
        }
    }
}

void FbxMeshLoader::Impl::_LoadNode(const FbxNode* node)
{
    if(node->GetNodeAttribute() == NULL)
    {
        FBXSDK_printf("NULL node Attribute\n\n");
    }
    else
    {
        FbxNodeAttribute::EType attributeType = (node->GetNodeAttribute()->GetAttributeType());

        switch (attributeType)
        {
//        case FbxNodeAttribute::eMarker:  
//            DisplayMarker(node);
//            break;
//
//        case FbxNodeAttribute::eSkeleton:  
//            DisplaySkeleton(node);
//            break;
//
        case FbxNodeAttribute::eMesh:
            {
                ResMeshData* meshData = new ResMeshData();
                const FbxMesh* mesh = reinterpret_cast<const FbxMesh*>(node->GetNodeAttribute());
                meshData->name = node->GetName();
                _LoadMesh(meshData, *mesh);

                meshArray.push_back(meshData);
            } break;

//        case FbxNodeAttribute::eNurbs:      
//            DisplayNurb(node);
//            break;
//
//        case FbxNodeAttribute::ePatch:     
//            DisplayPatch(node);
//            break;
//
//        case FbxNodeAttribute::eCamera:    
//            DisplayCamera(node);
//            break;
//
//        case FbxNodeAttribute::eLight:     
//            DisplayLight(node);
//            break;
//
//        case FbxNodeAttribute::eLODGroup:
//            DisplayLodGroup(node);
//            break;
        }
    }

//    DisplayUserProperties(node);
//    DisplayTarget(node);
//    DisplayPivotsAndLimits(node);
//    DisplayTransformPropagation(node);
//    DisplayGeometricTransform(node);

    for(int index = 0; index < node->GetChildCount(); ++index)
    {
        _LoadNode(node->GetChild(index));
    }
}

void FbxMeshLoader::Impl::_LoadMesh(ResMeshData* meshData, const FbxMesh& mesh)
{
    _LoadMeshVertices(meshData, mesh);
    _LoadMeshIndices(meshData, mesh);
}

void FbxMeshLoader::Impl::_LoadMeshVertices(ResMeshData* meshData, const FbxMesh& mesh)
{
    // vertex
    int verticesNum         = mesh.GetControlPointsCount();
    KFbxVector4* vertices   = mesh.GetControlPoints();

    meshData->vertices.resize(verticesNum);

    for( int index = 0; index < verticesNum; ++index)
    {
        KFbxVector4& v = vertices[index];
        res::Vertex& vertex = meshData->vertices[index];
        res::Vector& pos = vertex.pos;

        pos.x = static_cast<f32>(v[0]);
        pos.y = static_cast<f32>(v[1]);
        pos.z = static_cast<f32>(v[2]);
//        pos.w = v[3];

    }

    // normal
    int normalNumPerVertex = mesh.GetElementNormalCount();
    assert(normalNumPerVertex == 0 || normalNumPerVertex == 1);

    for(int normalIndex = 0; normalIndex < normalNumPerVertex; ++normalIndex)
    {
        const FbxGeometryElementNormal* normal = mesh.GetElementNormal(normalIndex);
        if (normal->GetMappingMode() == FbxGeometryElement::eByControlPoint
         && normal->GetReferenceMode() == FbxGeometryElement::eDirect)
        {
            for(int index = 0; index < verticesNum; ++index)
            {
                FbxVector4 v = normal->GetDirectArray().GetAt(index);
                res::Vertex& vertex = meshData->vertices[index];
                res::Vector& pos = vertex.normal;
        
                pos.x = static_cast<f32>(v[0]);
                pos.y = static_cast<f32>(v[1]);
                pos.z = static_cast<f32>(v[2]);
        //        pos.w = v[3];
            }
        }
    }
}

void FbxMeshLoader::Impl::_LoadMeshIndices(ResMeshData* meshData, const FbxMesh& mesh)
{
    // index
    int polygonNum = mesh.GetPolygonCount();
    int indicesNum = 0;

    meshData->polygonIndices.resize(polygonNum);
    meshData->indexNum = 3;

    for(int polyIndex = 0; polyIndex < polygonNum; ++polyIndex)
    {
        PolygonIndex& poly = meshData->polygonIndices[polyIndex];

        // index num per polygon
        int polygonVertexNum = mesh.GetPolygonSize(polyIndex);

        if(polygonVertexNum == 3)
        {
            poly.indices.resize(polygonVertexNum);

            for(int index = 0; index < polygonVertexNum; ++index)
            {
                poly.indices[index] = mesh.GetPolygonVertex(polyIndex, index);
            }
        }
        else if(polygonVertexNum == 4)
        {
            poly.indices.resize(6);
            _ConvMeshIndexQuad2Triangle(&poly, mesh, polyIndex);
        }
        indicesNum += poly.indices.size();

//        if(polyIndex == 0)
//        {
//            meshData->indexNum = polygonVertexNum;
//        }
//        else
//        {
//            assert(meshData->indexNum == polygonVertexNum);
//        }
    }
#if 0
    // normal
    int normalNum = 0;
    for(int index = 0; index < indicesNum; ++index)
    {
        int normalNumPerVertex = mesh.GetElementNormalCount();

        for(int normalIndex = 0; normalIndex < normalNumPerVertex; ++normalIndex)
        {
            const FbxGeometryElementNormal* normal = mesh.GetElementNormal(normalIndex);
            if (normal->GetMappingMode() == FbxGeometryElement::eByPolygonVertex)
            {
                normalNum += 1;
                assert( normal->GetReferenceMode() == FbxGeometryElement::eIndexToDirect );
            }
        }
    }

    for(int index = 0; index < normalNum; ++index)
    {
        int normalNumPerVertex = mesh.GetElementNormalCount();

        for(int normalIndex = 0; normalIndex < normalNumPerVertex; ++normalIndex)
        {
            const FbxGeometryElementNormal* normal = mesh.GetElementNormal(normalIndex);
            if (normal->GetMappingMode() == FbxGeometryElement::eByPolygonVertex)
            {
                FbxVector4 v = NULL;
                switch (normal->GetReferenceMode())
                {
                case FbxGeometryElement::eDirect:
                    v = normal->GetDirectArray().GetAt(index);
                    break;
                case FbxGeometryElement::eIndexToDirect:
                    int id = normal->GetIndexArray().GetAt(index);
                    v = normal->GetDirectArray().GetAt(id);
                    break;
                }
            }
        }
    }
#endif // 0
}

void FbxMeshLoader::Impl::_ConvMeshIndexQuad2Triangle(PolygonIndex* poly, const FbxMesh& mesh, int polyIndex)
{
    // 
    poly->indices[0] = mesh.GetPolygonVertex(polyIndex, 0);
    poly->indices[1] = mesh.GetPolygonVertex(polyIndex, 1);
    poly->indices[2] = mesh.GetPolygonVertex(polyIndex, 2);

    //
    poly->indices[3] = mesh.GetPolygonVertex(polyIndex, 0);
    poly->indices[4] = mesh.GetPolygonVertex(polyIndex, 2);
    poly->indices[5] = mesh.GetPolygonVertex(polyIndex, 3);
}

void FbxMeshLoader::Impl::_PrintMesh(const ResMeshData* meshData) const
{
    cout << "Mesh name : " << meshData->name.c_str() << endl;

    int verticesNum = meshData->vertices.size();
    cout << "Vertices : " << verticesNum << endl;
    for(int index = 0; index < verticesNum; ++index)
    {
        const res::Vertex& vertex = meshData->vertices[index];
        const res::Vector& pos = vertex.pos;;
        cout << " [" << index << "] : " << pos.x << ", " << pos.y << ", " << pos.z << endl;
    }
    cout << "Normals : " << verticesNum << endl;
    for(int index = 0; index < verticesNum; ++index)
    {
        const res::Vertex& vertex = meshData->vertices[index];
        const res::Vector& pos = vertex.normal;
        cout << " [" << index << "] : " << pos.x << ", " << pos.y << ", " << pos.z << endl;
    }

    int polygonNum = meshData->polygonIndices.size();
    cout << "polygonNum : " << polygonNum << endl;
    for(int index = 0; index < polygonNum; ++index)
    {
        const PolygonIndex& polygon = meshData->polygonIndices[index];
        int polyIndex = polygon.indices.size();
        cout << " [" << polyIndex << "] : ";
        for(int pindex = 0; pindex < polyIndex; ++pindex)
        {
            cout << " " << polygon.indices[pindex] << ", ";
        }
        cout << endl;
    }
}


