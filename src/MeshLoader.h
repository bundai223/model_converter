//**********************************************************************
//! @file   MeshLoader.h
//! @brief  describe
//**********************************************************************
#ifndef _MESHLOADER_H_INCLUDED_
#define _MESHLOADER_H_INCLUDED_

#include "FbxMeshLoaderRes.h"

class IMeshLoader
{
public:
    IMeshLoader(){}
    virtual ~IMeshLoader(){}

    virtual void LoadMesh(const char* filename) = 0;

    virtual u32                         MeshNum()       const = 0;
    virtual u32                         MaterialNum()   const = 0;
    virtual u32                         BoneNum()       const = 0;

    virtual const ResMeshData*          Mesh(s32 index)     const = 0;
    virtual const res::MaterialData*    Material(s32 index) const = 0;
    virtual const res::BoneData*        Bone(s32 index)     const = 0;
private:
};


#endif // _MESHLOADER_H_INCLUDED_

