//**********************************************************************
//! @file   fbxLoader.cpp
//! @brief  describe
//**********************************************************************
#include <iostream>
#include "fbxLoader.h"
#include "FbxMeshLoader.h"
#include "Common/Common.h"

using namespace std;

void PrintModel(KFbxScene* scene);
void PrintNode(FbxNode* node);
void PrintMesh(FbxNode* node);
void PrintMeshVertices(FbxMesh* mesh);
void PrintMeshIndices(FbxMesh* mesh);


void fbxload(const char* fbxFileName)
{
    FbxMeshLoader loader;
    loader.LoadMesh(fbxFileName);
}

void test_fbxload(const char* fbxFileName)
{
    parseFBX(fbxFileName);
}

void parseFBX(const char* fbxFileName)
{
    KFbxSdkManager* sdkManager = NULL;
    KFbxScene* scene = NULL;

    InitializeSdkObjects(sdkManager, scene);

    bool result = LoadScene(sdkManager, scene, fbxFileName);

    PrintModel(scene);

    scene->Destroy();
    DestroySdkObjects(sdkManager, true);
}

void PrintModel(KFbxScene* scene)
{
    FbxNode* node = scene->GetRootNode();
    
    if(node != NULL)
    {
        const int CHILD_NUM = node->GetChildCount();
        for(int index = 0; index < CHILD_NUM; ++index)
        {
            PrintNode(node->GetChild(index));
        }
    }
}

void PrintNode(FbxNode* node)
{
    if(node->GetNodeAttribute() == NULL)
    {
        FBXSDK_printf("NULL node Attribute\n\n");
    }
    else
    {
        FbxNodeAttribute::EType attributeType = (node->GetNodeAttribute()->GetAttributeType());

        switch (attributeType)
        {
//        case FbxNodeAttribute::eMarker:  
//            DisplayMarker(node);
//            break;
//
//        case FbxNodeAttribute::eSkeleton:  
//            DisplaySkeleton(node);
//            break;
//
        case FbxNodeAttribute::eMesh:      
            PrintMesh(node);
            break;

//        case FbxNodeAttribute::eNurbs:      
//            DisplayNurb(node);
//            break;
//
//        case FbxNodeAttribute::ePatch:     
//            DisplayPatch(node);
//            break;
//
//        case FbxNodeAttribute::eCamera:    
//            DisplayCamera(node);
//            break;
//
//        case FbxNodeAttribute::eLight:     
//            DisplayLight(node);
//            break;
//
//        case FbxNodeAttribute::eLODGroup:
//            DisplayLodGroup(node);
//            break;
        }
    }

//    DisplayUserProperties(node);
//    DisplayTarget(node);
//    DisplayPivotsAndLimits(node);
//    DisplayTransformPropagation(node);
//    DisplayGeometricTransform(node);

    for(int index = 0; index < node->GetChildCount(); ++index)
    {
        PrintNode(node->GetChild(index));
    }
}

void PrintMesh(FbxNode* node)
{
    FbxMesh* mesh = (FbxMesh*) node->GetNodeAttribute();
    cout << "Mesh name : " << node->GetName() << endl;

    PrintMeshVertices(mesh);
    PrintMeshIndices(mesh);
}

void PrintMeshVertices(FbxMesh* mesh)
{
    // vertex
    int verticesNum         = mesh->GetControlPointsCount();
    KFbxVector4* vertices   = mesh->GetControlPoints();

    cout << "Vertices : " << verticesNum << endl;
    for( int index = 0; index < verticesNum; ++index)
    {
        KFbxVector4& v = vertices[index];
        cout << " [" << index << "] : " << v[0] << ", " << v[1] << ", " << v[2] << ", " << v[3] << endl;
    }

    // normal
    int normalNum = verticesNum;

    cout << "Vertices Normals : " << normalNum << endl;
    for(int index = 0; index < normalNum; ++index)
    {
        int normalNumPerVertex = mesh->GetElementNormalCount();

        for(int normalIndex = 0; normalIndex < normalNumPerVertex; ++normalIndex)
        {
            FbxGeometryElementNormal* normal   = mesh->GetElementNormal(normalIndex);
            if (normal->GetMappingMode() == FbxGeometryElement::eByControlPoint)
            {
                if (normal->GetReferenceMode() == FbxGeometryElement::eDirect)
                {
                    FbxVector4 v = normal->GetDirectArray().GetAt(index);
                    cout << " [" << index << "] : " << v[0] << ", " << v[1] << ", " << v[2] << ", " << v[3] << endl;
                }
            }
        }
    }
}

void PrintMeshIndices(FbxMesh* mesh)
{
    // index
    int polygonNum = mesh->GetPolygonCount();
    int indicesNum = 0;
    cout << "polygonNum : " << polygonNum << endl;
    for(int polyIndex = 0; polyIndex < polygonNum; ++polyIndex)
    {
        cout << " [" << polyIndex << "] : ";
        // index num per polygon
        int polygonVertexNum = mesh->GetPolygonSize(polyIndex);
        for(int indexIndex = 0; indexIndex < polygonVertexNum; ++indexIndex)
        {
            int index = mesh->GetPolygonVertex(polyIndex, indexIndex);
            cout << " " << index << ", ";
        }
        cout << endl;

        indicesNum += polygonVertexNum;
    }

    // normal
    int normalNum = 0;
    for(int index = 0; index < indicesNum; ++index)
    {
        int normalNumPerVertex = mesh->GetElementNormalCount();

        for(int normalIndex = 0; normalIndex < normalNumPerVertex; ++normalIndex)
        {
            FbxGeometryElementNormal* normal   = mesh->GetElementNormal(normalIndex);
            if (normal->GetMappingMode() == FbxGeometryElement::eByPolygonVertex)
            {
                normalNum += 1;
            }
        }
    }

    cout << "Vertices Normals : " << normalNum << endl;
    for(int index = 0; index < normalNum; ++index)
    {
        int normalNumPerVertex = mesh->GetElementNormalCount();

        for(int normalIndex = 0; normalIndex < normalNumPerVertex; ++normalIndex)
        {
            FbxGeometryElementNormal* normal   = mesh->GetElementNormal(normalIndex);
            if (normal->GetMappingMode() == FbxGeometryElement::eByPolygonVertex)
            {
                FbxVector4 v = NULL;
                switch (normal->GetReferenceMode())
                {
                case FbxGeometryElement::eDirect:
                    v = normal->GetDirectArray().GetAt(index);
                    break;
                case FbxGeometryElement::eIndexToDirect:
                    int id = normal->GetIndexArray().GetAt(index);
                    v = normal->GetDirectArray().GetAt(id);
                    break;
                }
                cout << " [" << index << "] : " << v[0] << ", " << v[1] << ", " << v[2] << ", " << v[3] << endl;
            }
        }
    }
}


