//**********************************************************************
//! @file   MeshOutput.h
//! @brief  describe
//**********************************************************************
#ifndef _MESHOUTPUT_H_INCLUDED_
#define _MESHOUTPUT_H_INCLUDED_

class IMeshLoader;

namespace MeshOutput {

    void Output(const char* filename, const IMeshLoader& loader);

} // namespace MeshOutput


#endif // _MESHOUTPUT_H_INCLUDED_

