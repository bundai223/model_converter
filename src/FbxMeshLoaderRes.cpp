//**********************************************************************
//! @file   FbxMeshLoaderRes.cpp
//! @brief  describe
//**********************************************************************
#include "FbxMeshLoaderRes.h"
#include "mlUtil.h"


u32 CalcMeshSize(const ResMeshData& mesh)
{
    // mesh num + material num
    u32 size = sizeof(u32) * 3;
    // name
    size += sizeof(u32);
    size += ML_ALIGNMENT(mesh.name.size() + 1, 4);

    // vertex
    size += sizeof(res::Vertex) * mesh.vertices.size();

    // index
    u32 polyNum = mesh.polygonIndices.size();
    for(u32 index = 0; index < polyNum; ++index)
    {
        const PolygonIndex& poly = mesh.polygonIndices[index];
        size += sizeof(u32) * poly.indices.size();
    }
    return size;
}


