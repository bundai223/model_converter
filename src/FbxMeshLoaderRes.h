//**********************************************************************
//! @file   FbxMeshLoaderRes.h
//! @brief  describe
//**********************************************************************
#ifndef _FBXMESHLOADERRES_H_INCLUDED_
#define _FBXMESHLOADERRES_H_INCLUDED_

#include <vector>
#include <string>
#include "res/ResModel.h"
#include "res/ResMesh.h"
#include "res/ResMaterial.h"
#include "res/ResSkeleton.h"
#include "res/ResBone.h"

struct PolygonIndex
{
    std::vector<u32>            indices;
};

struct ResMeshData
{
    u32                         indexNum;
    std::string                 name;
    std::vector<res::Vertex>    vertices;
    std::vector<PolygonIndex>   polygonIndices;
};

u32 CalcMeshSize(const ResMeshData& mesh);
//u32 CalcMaterialSize(const ResMeshData& mesh);


#endif // _FBXMESHLOADERRES_H_INCLUDED_

