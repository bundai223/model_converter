//**********************************************************************
//! @file   FbxMeshLoader.h
//! @brief  describe
//**********************************************************************
#ifndef _FBXMESHLOADER_H_INCLUDED_
#define _FBXMESHLOADER_H_INCLUDED_

#include <memory>
#include "MeshLoader.h"


class FbxMeshLoader : public IMeshLoader
{
public:
    FbxMeshLoader(void);
    virtual ~FbxMeshLoader();

    virtual void LoadMesh(const char* filename);

    virtual u32                         MeshNum() const;
    virtual u32                         MaterialNum() const;
    virtual u32                         BoneNum() const;

    virtual const ResMeshData*          Mesh(s32 index) const;
    virtual const res::MaterialData*    Material(s32 index) const;
    virtual const res::BoneData*        Bone(s32 index) const;
private:
    void    _Initialize(void);
    void    _Finalize(void);

    class Impl;
    std::auto_ptr<Impl> mImpl;
};



#endif // _FBXMESHLOADER_H_INCLUDED_

