//**********************************************************************
//! @file   fbxLoader.h
//! @brief  describe
//**********************************************************************
#ifndef _FBXLOADER_H_INCLUDED_
#define _FBXLOADER_H_INCLUDED_


extern void fbxload(const char* fbxFileName);
extern void test_fbxload(const char* fbxFileName);

extern void parseFBX(const char* fbxFileName);


#endif // _FBXLOADER_H_INCLUDED_

