//
//  main.cpp
//  fbx
//
//  Created by Daiji Nishimura on 2013/02/27.
//  Copyright (c) 2013年 Daiji Nishimura. All rights reserved.
//

#include <iostream>
#include "Common/Common.h"
//#include "fbxLoader.h"
#include "MeshOutput.h"
#include "FbxMeshLoader.h"


enum Arg
{
    ARG_EXE=0,
    ARG_FBX,
    ARG_DST_PATH,
    
    ARG_NUM
};


// 
void PrintArgs(int count, const char* argv[]);
void PrintUsage();
void ConvertFbx(const char* output, const char* input)
{
    FbxMeshLoader loader;
    loader.LoadMesh(input);
    MeshOutput::Output(output, loader);
}



int main(int argc, const char * argv[])
{
    PrintArgs(argc, argv);

    if(argc == ARG_NUM)
    {
        const char* fbxFileName     = argv[ARG_FBX];
        const char* outputFileName  = argv[ARG_DST_PATH];

        ConvertFbx(outputFileName, fbxFileName);
    }
    else
    {
        PrintUsage();
    }
    return 0;
}


void PrintArgs(int count, const char* argv[])
{
    FBXSDK_printf("***************\n");
    for(int i = 0; i < count; ++i)
    {
        FBXSDK_printf("%3d : %s\n", i, argv[i]);
    }
    FBXSDK_printf("***************\n");
}

void PrintUsage()
{
    FBXSDK_printf( "Usage: model_loader <FBX file name> <output file path>\n" );
}

