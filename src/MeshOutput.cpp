//**********************************************************************
//! @file   MeshOutput.cpp
//! @brief  describe
//**********************************************************************
#include <stdlib.h>
#include "MeshOutput.h"
#include "MeshLoader.h"
#include "mlUtil.h"


namespace MeshOutput {

    //********************************************************************************
    //! @brief	write file
    //********************************************************************************
    bool    WriteFile( const u8* pBuffer, u32 nFileSize, const char* szFileName )
    {
//        ML_NULL_ASSERT( pBuffer );
//        ML_ASSERT( 0 < nFileSize, "file size is wrong" );
//        ML_NULL_ASSERT( szFileName );

        FILE* pFile = fopen( szFileName, "w" );
//        ML_ASSERT( pFile != NULL, "cannot file open : %s", szFileName );
        if( pFile == NULL )
        {
            return false;
        }

    	// load file
        size_t nWriteNum = fwrite( pBuffer, nFileSize, 1, pFile );
//        ML_ASSERT( nWriteNum == 1, "cannot write : %s", szFileName );

        bool result = (nWriteNum == 1);
        fclose( pFile );

        return result;
    }


    //********************************************************************************
    //! @brief	file load
    //********************************************************************************
    void*    LoadFile( u32* pFileSize, const char* szFileName )
    {
//        ML_NULL_ASSERT( szFileName );
//
        FILE* pFile = fopen( szFileName, "rb" );
//        ML_ASSERT( pFile != NULL, "cannot file open : %s", szFileName );
        if( pFile == NULL )
        {
            return NULL;
        }

        // get file size
        fpos_t fpos;
        fseek( pFile, 0, SEEK_END );
        fgetpos( pFile, &fpos );
        fseek( pFile, 0, SEEK_SET );
    #if defined(WIN32) || defined(__APPLE_CC__)
        size_t nFileSize = static_cast<size_t>( fpos );
    #else
        size_t nFileSize = static_cast<size_t>( fpos.__pos );
    #endif

        // alloc file buffer
        void* pBuf = reinterpret_cast<u8*>( malloc( nFileSize ) );
//        ML_NULL_ASSERT( pBuf );
        if( pBuf == NULL )
        {
            fclose( pFile );
            return NULL;
        }

        // load file
        size_t nReadNum = fread( pBuf, nFileSize, 1, pFile );
//        ML_ASSERT( nReadNum == 1, "cannot file read : %s", szFileName );
        if( nReadNum != 1 )
        {
            SafeFree( pBuf );
            fclose( pFile );
            return NULL;
        }

        fclose( pFile );

        if( pFileSize != NULL )
        {
            *pFileSize = static_cast<u32>( nFileSize );
        }

        return pBuf;
    }

    u32 GetIndexNum(const ResMeshData& meshData)
    {
        u32 polyNum = meshData.polygonIndices.size();
        u32 indexNum = 0;
        for(u32 i = 0; i < polyNum; ++i)
        {
            const PolygonIndex& poly = meshData.polygonIndices[i];
            u32 polyIndexNum = poly.indices.size();
            indexNum += polyIndexNum;
        }

        return indexNum;
    }


    // 
    u32 OutputModel(void* ptr, const IMeshLoader& loader);
    // mesh
    u32 OutputMeshAll(void* ptr, const IMeshLoader& loader);
    u32 OutputMesh(void* ptr, const ResMeshData& meshData);
    u32 OutputVertexAll(void* ptr, const ResMeshData& meshData);
    u32 OutputVertex(void* ptr, const res::Vertex& vertex);
    u32 OutputIndexAll(void* ptr, const ResMeshData& meshData);
    // material
    u32 OutputMaterialAll(void* ptr, const IMeshLoader& loader);
//    u32 OutputMesh(void* ptr, const ResMeshData& meshData);

    //! @brief	Write memory
    //! @param	dst[out]	destination memory
    //! @param	src[in]		source memory
    //! @param	srcSize[in]	source memory size
    //! @retval	void*		next destination memory ptr
    void*   WritePtr(void* dst, const void* src, u32 srcSize)
    {
        memcpy(dst, src, srcSize);
        return OffsetPtr(dst, srcSize);
    }
    void*   WritePtrU32(void* dst, u32 src)
    {
        return WritePtr(dst, &src, sizeof(u32));
    }

    u32 CalcModelSize(const IMeshLoader& loader)
    {
        u32 meshNum = loader.MeshNum();
        // TODO: material
        u32 materialNum = loader.MaterialNum();
        u32 size = sizeof(u32) * 2;
        for(u32 index = 0; index < meshNum; ++index)
        {
            const ResMeshData* meshData = loader.Mesh(index);
            u32 meshSize = CalcMeshSize(*meshData);
            size += meshSize;
        }
        return size;
    }

    void Output(const char* filename, const IMeshLoader& loader)
    {
        u32 calcMeshSize = CalcModelSize(loader);
        void* buffer = malloc(calcMeshSize);

        u32 meshSize = OutputModel(buffer, loader);
        printf("calc size : %d\n", calcMeshSize);
        printf("real size : %d\n", meshSize);

        // output for file
        WriteFile(reinterpret_cast<u8*>(buffer), calcMeshSize, filename);

        // TODO: test
        {
            u32 fileSize = 0;
            void* ptr = LoadFile(&fileSize, "testData.mdlb");
//            res::ModelData* model = reinterpret_cast<res::ModelData*>(ptr);
            res::ModelData model; 
            model.Setup(ptr);

            Dump(model);

            SafeFree(ptr);
        }

        SafeFree(buffer);
    }

    u32 OutputModel(void* ptr, const IMeshLoader& loader)
    {
        u32 totalSize = 0;

        totalSize = sizeof(u32);
        ptr = WritePtrU32(ptr, loader.MeshNum());

        totalSize += sizeof(u32);
        ptr = WritePtrU32(ptr, loader.MaterialNum());

        u32 meshSize = OutputMeshAll(ptr, loader);
        ptr = OffsetPtr(ptr, meshSize);
        totalSize += meshSize;

        u32 matSize = OutputMaterialAll(ptr, loader);
        ptr = OffsetPtr(ptr, matSize);
        totalSize += matSize;

        return totalSize;
    }

    u32 OutputMeshAll(void* ptr, const IMeshLoader& loader)
    {
        u32 totalSize = 0;
        u32 meshNum = loader.MeshNum();
        for(u32 index = 0; index < meshNum; ++index)
        {
            const ResMeshData* meshData = loader.Mesh(index);
            u32 size = OutputMesh(ptr, *meshData);

            ptr = OffsetPtr(ptr, size);
            totalSize += size;
        }

        return totalSize;
    }

    u32 OutputMesh(void* ptr, const ResMeshData& meshData)
    {
        u32 totalSize = 0;

        // mesh member
        ptr = WritePtrU32(ptr, meshData.indexNum);
        ptr = WritePtrU32(ptr, meshData.vertices.size());
        ptr = WritePtrU32(ptr, GetIndexNum(meshData));
//        ptr = WritePtrU32(ptr, meshData.polygonIndices.size());

        totalSize += sizeof(u32) * 3;

        // name
        u32 writeNameSize = static_cast<u32>(ML_ALIGNMENT(meshData.name.size() + 1, 4));
        ptr = WritePtrU32(ptr, writeNameSize);
        totalSize += sizeof(u32);

        u32 nameSize = meshData.name.size();
        ptr = WritePtr(ptr, meshData.name.c_str(), nameSize);
        {   // set zero for padding
            char* namePtr = reinterpret_cast<char*>(ptr);
            u32 remainSize = writeNameSize - nameSize;
            for(u32 index = 0; index < remainSize; ++index)
            {
                namePtr[index] = '\0';
            }
            ptr = OffsetPtr(ptr, remainSize);
        }
        totalSize += writeNameSize;

        // vertex
        u32 vertexSize = OutputVertexAll(ptr, meshData);
        ptr = OffsetPtr(ptr, vertexSize);
        totalSize += vertexSize;

        // index
        u32 indexSize = OutputIndexAll(ptr, meshData);
        ptr = OffsetPtr(ptr, indexSize);
        totalSize += indexSize;

        return totalSize;
    }

    u32 OutputVertexAll(void* ptr, const ResMeshData& meshData)
    {
        u32 totalSize = 0;
        u32 vertexNum = meshData.vertices.size();

        for(u32 index = 0; index < vertexNum; ++index)
        {
            u32 vertexSize = OutputVertex(ptr, meshData.vertices[index]);
            ptr = OffsetPtr(ptr, vertexSize);
            totalSize += vertexSize;
        }

        return totalSize;
    }

    u32 OutputVertex(void* ptr, const res::Vertex& vertex)
    {
        u32 totalSize = sizeof(res::Vertex);
        ptr = WritePtr(ptr, &vertex, totalSize);

        return totalSize;
    }

    u32 OutputIndexAll(void* ptr, const ResMeshData& meshData)
    {
        u32 polyNum = meshData.polygonIndices.size();
        u32 indexNum = 0;
        for(u32 i = 0; i < polyNum; ++i)
        {
            const PolygonIndex& poly = meshData.polygonIndices[i];
            u32 polyIndexNum = poly.indices.size();
            for(u32 j = 0; j < polyIndexNum; ++j)
            {
                ptr = WritePtrU32(ptr, poly.indices[j]);
            }
            indexNum += polyIndexNum;
        }

        u32 totalSize = indexNum * sizeof(u32);
        return totalSize;
    }

    u32 OutputMaterialAll(void* ptr, const IMeshLoader& loader)
    {
        return 0;
    }

} // namespace MeshOutput

