#
#
# Required BUILD_DIR
#
BUILD_DIR=./build

usage:
	@echo "usage: make platform"
	@echo " platform:"
	@echo "  xcode    : create XCode project"
	@echo "  makefile : create Makefile"
	@echo "  vc2010   : create Visual Studio project"

xcode:
	@(cd $(BUILD_DIR);cmake -G "Xcode" ..)

makefile:
	@(cd $(BUILD_DIR);cmake -G "Unix Makefiles" ..)

vc2010:
	@(cd $(BUILD_DIR);cmake -G "Visual Studio 10" ..)

vc2008:
	@(cd $(BUILD_DIR);cmake -G "Visual Studio 9 2008" ..)

clean:
	@(cd $(BUILD_DIR); rm -rf *)
	@echo clean


.PHONY: clean xcode makefile vc2010
